The notebook `Reconstruction_in_CT` examines 4 reconstruction algorithms - two algebraic and two iterative and looks at some of the differences and how they can be improved. The algorithms are:

* Algebraic
    * Radon Transform Algorithm
    * Projection Slice Theorem Reconstruction
* Iterative
    * SART
    *  ISRA

This notebook assumes that the reader has basic knowledge of linear algebra and Fourier transform as they are not covered.
